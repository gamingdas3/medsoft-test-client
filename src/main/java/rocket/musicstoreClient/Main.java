package rocket.musicstoreClient;

import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.scene.Scene;
import retrofit2.*;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import rocket.musicstoreClient.POJO.CartItem;
import rocket.musicstoreClient.POJO.Order;
import rocket.musicstoreClient.POJO.OrderItem;
import rocket.musicstoreClient.POJO.Product;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main extends Application {
    public class SessionData {
        private String token;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

    }
    public class Controller {

        private MSService API;
        private URI staticUrlBase;
        private SessionData session;

        private Scene Scene;

        private VBox MainBox = new VBox();
        private VBox LoginBox = new VBox();
        private GridPane StoreBox = new GridPane();
        private VBox StoreBoxWrapper = new VBox();
        private VBox CartBox = new VBox();



        Button BtnSignup = new Button("Register");
        Button BtnAuth = new Button("Authorise");
        TextField TextPass = new TextField();
        TextField TextLogin = new TextField();

        Button BtnCart = new Button("Show me my cart");


        Controller(MSService api, URI staticUrl, SessionData session){

            API = api;
            staticUrlBase = staticUrl;
            this.session = session;

            ConstructScene();
            SetEventHandlers();

        }

        private void ConstructScene(){
            LoginBox.setSpacing(2);
            TextLogin.setText("login");
            TextPass.setText("pass");

            StoreBoxWrapper.setSpacing(10);
            StoreBoxWrapper.getChildren().addAll(BtnCart, StoreBox);

            LoginBox.getChildren().addAll(TextLogin, TextPass, BtnSignup, BtnAuth);
            MainBox.getChildren().addAll(LoginBox);
            Scene = new Scene(MainBox);
        }

        private void SetEventHandlers()
        {

            BtnSignup.setOnAction(event -> {
                if(SignUp()){
                    ShowCatalog();
                }
            });

            BtnAuth.setOnAction(event -> {
                if(Authorise()) {
                    ShowCatalog();
                }
            });

            BtnCart.setOnAction(event -> {
                ShowCart();
            });

        }

        private VBox BuildProductCard(Product product){
            var card = new VBox();
            var url = staticUrlBase.resolve(product.getPath()).toString();


            var categoryBox = new Label(product.getCategory().getName());
            var nameBox = new Label(product.getName());
            var imageBox = new ImageView(new Image(url, 0, 150, true, false));
            var costBox = new Label(String.valueOf(product.getCost()));
            var addToCartBtn = new Button("+ to cart");
            addToCartBtn.setOnAction(event -> {

                if(AddToCart(product.getId())){

                    addToCartBtn.setDisable(true);

                }else{

                    addToCartBtn.setDisable(true);
                }
            });

            card.getChildren().addAll(categoryBox, nameBox, imageBox, costBox, addToCartBtn);

            return card;
        }

        private VBox BuildCartView(List<CartItem> cartItems){
            VBox root = new VBox();

            for (CartItem item: cartItems) {
                HBox row = new HBox();

                var url = staticUrlBase.resolve(item.getPath()).toString();
                var imageBox = new ImageView(new Image(url, 128, 0, true, false));

                Label lName = new Label(item.getName());
                Label lCost = new Label(String.valueOf( item.getCost() ) );
                Label lCnt = new Label(String.valueOf( item.getCount() ) );


                row.setSpacing(4);
                row.getChildren().addAll(imageBox, lName, lCost, lCnt);
                root.getChildren().add(row);
            }
            double sum = cartItems
                    .stream()
                    .mapToDouble( o -> o.getCost() * o.getCount())
                    .sum();

            var lSum = new Label(String.valueOf( sum ) );
            var btnCheckout = new Button("Proceed to checkout ->");
            var btnBackToMag = new Button("<- Back to store");

            btnCheckout.setOnAction(x -> Checkout());
            btnBackToMag.setOnAction(x -> ShowCatalog());

            root.getChildren().addAll(lSum, btnCheckout);
            return root;
        }


        private void FillStoreBox(){
            try {
                List<Product> products = API.GetProducts().execute().body()
                        .stream()
                        .filter(o -> o.getOrdered() < o.getStock())
                        .collect(Collectors.toList());

                int currentColIdx = 0;
                int currentRowIdx = 1;
                int colspan = 2;
                int rowspan = 3;
                int colLimit = 6;
                for (Product p: products) {
                    StoreBox.add(BuildProductCard(p), currentColIdx, currentRowIdx, colspan, rowspan);
                    currentColIdx += colspan;
                    if(currentColIdx >= colLimit){
                        currentColIdx = 0;
                        currentRowIdx += rowspan;
                    }
                }
            }
            catch (Exception ex){
                System.out.println(ex.toString());
            }
        }


        private void SetMainBoxContent(Node content){
            MainBox.getChildren().remove(0);
            MainBox.getChildren().add(content);
        }

        private boolean TrySetToken(Response<Map<String, String>> call) {
            try{

                var token = call.body().get("token");
                if(token==null) throw new Exception("No token received, there's probably an error in response body");
                this.session.setToken(token);

                return true;
            }
            catch (Exception ex){
                var errorText = call.body().get("error");
                new Alert(Alert.AlertType.ERROR, errorText).showAndWait();
                return  false;
            }
        }

        private boolean Authorise(){
            var loginText = TextLogin.getText();
            var passText = TextPass.getText();
            try {
                var call = API.Token(loginText, passText).execute();
                return TrySetToken(call);
            }
            catch (Exception ex){
                new Alert(Alert.AlertType.ERROR, ex.toString()).showAndWait();
                return false;
            }
        }

        private void ShowCatalog(){
            FillStoreBox();
            SetMainBoxContent(StoreBoxWrapper);
        }

        private boolean SignUp(){
            var loginText = TextLogin.getText();
            var passText = TextPass.getText();
            try {
                var call = API.SignUp(loginText, passText).execute();
                return TrySetToken(call);
            }
            catch (Exception ex){
                new Alert(Alert.AlertType.ERROR, ex.toString()).showAndWait();
                return false;
            }
        }

        private boolean AddToCart(int id){
            try {

                var body = API.AddToCart(this.session.getToken(), id, 1).execute().body();
                var result = body.get("result");
                if(result == null) throw new Exception("result is null");

                if(result.contains("success")){

                    new Alert(
                            Alert.AlertType.CONFIRMATION,
                            "Instrument added to rent cart."
                    ).showAndWait();

                    return true;

                }
                else{

                    var details = body.get("details");
                    new Alert(Alert.AlertType.ERROR,
                            ( details == null )
                                    ? result
                                    : result.concat(" : ").concat(details)
                    ).showAndWait();

                    return false;
                }

            }
            catch (Exception ex){
                new Alert(Alert.AlertType.ERROR, ex.toString())
                    .showAndWait();
                return false;
            }
        }

        private void ShowCart(){
            try {
                var cart = API.GetCart(this.session.getToken()).execute().body();
                CartBox.getChildren().removeAll();
                CartBox.getChildren().add(BuildCartView(cart));
                SetMainBoxContent(CartBox);
            }
            catch (Exception ex){
                new Alert(Alert.AlertType.ERROR, ex.toString()).showAndWait();
            }
        }

        private void Checkout(){
            try {
                var order = API.Checkout(this.session.getToken()).execute().body();
                new Alert(
                    Alert.AlertType.INFORMATION,
                    "Order was constructed, total sum is: ".concat( String.valueOf(order.getSum()) )
                )
                .showAndWait();
            }
            catch (Exception ex){
                new Alert(Alert.AlertType.ERROR, ex.toString()).showAndWait();
            }
        }

    }


    private URI staticUrl = URI.create("http://127.0.0.1:8080/static/musicstore/");
    private URI baseUrl = URI.create("http://127.0.0.1:8080/musicstore-1.0/");

    private MSService api;
    private Controller controller;
    private SessionData session;

    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) {
        try {
            //Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("Main.fxml"));
            URI staticUrlFromFS = URI.create("file:///C:/wildfly-16.0.0.Final/static/musicstore/");



            SetupServices();

            session = new SessionData();
            controller = new Controller(api, staticUrlFromFS, session);


            primaryStage.setScene(controller.Scene);
            primaryStage.setTitle("Rocket's Musicstore");
            primaryStage.setWidth(800);
            primaryStage.setHeight(600);

            primaryStage.show();
        }
        catch (Exception ex){System.out.println(ex.toString()); primaryStage.close();}
    }

    private void SetupServices(){


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl.resolve("rest/").toString())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api = retrofit.create(MSService.class);
    }



}