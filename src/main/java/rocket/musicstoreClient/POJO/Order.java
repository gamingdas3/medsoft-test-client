
package rocket.musicstoreClient.POJO;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("items")
    @Expose
    private List<OrderItem> orderItems = null;
    @SerializedName("sum")
    @Expose
    private double sum;
    @SerializedName("isPaid")
    @Expose
    private boolean isPaid;
    @SerializedName("isRetained")
    @Expose
    private boolean isRetained;

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public boolean isIsPaid() {
        return isPaid;
    }

    public void setIsPaid(boolean isPaid) {
        this.isPaid = isPaid;
    }

    public boolean isRetained() {
        return isRetained;
    }

    public void setRetained(boolean retained) {
        isRetained = retained;
    }
}
