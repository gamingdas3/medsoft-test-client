package rocket.musicstoreClient;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rocket.musicstoreClient.POJO.CartItem;
import rocket.musicstoreClient.POJO.Order;
import rocket.musicstoreClient.POJO.Product;

public interface MSService {

    @GET("signup")
    @Headers({
            "Accept: application/json"
    })
    Call<Map<String, String>> SignUp (
            @Query("login") String login,
            @Query("pass") String pass
    );

    @GET("token")
    @Headers({
            "Accept: application/json"
    })
    Call<Map<String, String>> Token (
            @Query("login") String login,
            @Query("pass") String pass
    );

    @GET("categoriesList")
    @Headers({
            "Accept: application/json"
    })
    Call<String> GetCategories ();

    @GET("catalog")
    @Headers({
            "Accept: application/json"
    })
    Call<List<Product>> GetProducts ();

    @GET("addtocart")
    @Headers({
            "Accept: application/json"
    })
    Call<Map<String, String>> AddToCart(
            @Query("token") String token,
            @Query("id") int id,
            @Query("count") int count
    );

    @GET("cart")
    @Headers({
            "Accept: application/json"
    })
    Call<List<CartItem>> GetCart(
            @Query("token") String token
    );

    @GET("checkout")
    @Headers({
            "Accept: application/json"
    })
    Call<Order> Checkout(
            @Query("token") String token
    );
}

    //@Path("user") String user
