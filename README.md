    # Test application "Musicstore" client

Application built from an example of how to set up Java 12, JavaFX and Gradle application.

## Features:

  * Register and login 
  * View catalog of products filtered by availability   
  * Add product to cart and see various error messages in cases of violation of terms of use
  * Create order from cart items and see total sum of the order

## Gradle Commands
run in place:

    ./gradlew run 

Build a distribution zip in `build/distributions`:

    ./gradlew distZip


## Dependencies:
  * [Open JDK 12](https://adoptopenjdk.net/?variant=openjdk12&jvmVariant=hotspot)
  * [Gradle 5.4.1](https://gradle.org/install/)
  * JavaFX 12 (Will be downloaded by Gradle)


## Background
I had some trouble setting this example up, but it turns out that the 
[org.openjfx.javafxplugin Gradle plugin](https://github.com/openjfx/javafx-gradle-plugin)
works great.

I also have an 
[earlier branch of this example](https://github.com/pelamfi/gradle-javafx-hello-world-app/tree/gradle-javafx-run-working-without-plugin-and-with-hacks) 
that reaches the same goals without using the JavaFX Gradle
plugin, but that seems to require some strange hacks that the plugin probably takes care of.
